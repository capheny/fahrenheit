package sheridan;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 * @author Yiyao Zhang
 */

class FahrenheitTest {

    @Test
    void fromCelsiusRegular() {
        int fahrenheit = Fahrenheit.fromCelsius(30);
        assertEquals(fahrenheit, 86);
    }

    @Test
    void fromCelsiusExceptional() {
        assertThrows(IllegalArgumentException.class, () -> {
            int dummy = Fahrenheit.fromCelsius(-276);
        });
    }

    @Test
    void fromCelsiusBoundaryIn() {
        int fahrenheit = Fahrenheit.fromCelsius(-269);
        assertEquals(fahrenheit, -452);
    }

    @Test
    void fromCelsiusBoundaryOut() {
        int fahrenheit = Fahrenheit.fromCelsius(100);
        assertEquals(fahrenheit, 212);
    }

}