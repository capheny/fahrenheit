package sheridan;

public class Fahrenheit {
    public static int fromCelsius(int degree) throws IllegalArgumentException {
        if ((double) degree < -273.15) {
            throw new IllegalArgumentException("Input Celsius cannot be less than -273.15");
        }

        double fahrenheit = (double) degree * 9.0 / 5.0 + 32;
        if (fahrenheit % 1 < 0.5) {
            return (int) fahrenheit;
        } else {
            return (int) fahrenheit + 1;
        }
    }
}
